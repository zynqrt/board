# Аппаратное и программное обеспечение

## Железо

* ARM Cortex-A9, 2 ядра
* Тактовая частота 667 МГц
* ОЗУ: 1 Гбайт, DDR3
* ПЗУ: SD card 8 Гбайт
* Сеть: Ethernet 10/100/1000
* Mini-USB - UART
 
## Программное обеспечение

Операционаая система Debian 10, ядро 4.19.55 (??) с патчем реального времени Xenomai.

Программы условно делим на 3 класса:

1. ПО реального времени (ПОРВ) для управлениями двигателями, сканерами, принтером.
2. ПО распознавания билетов (ПОРБ) и передачи на стойку.
3. ПО формирования билета (ПОФБ) и передачи в ПО реального времени.
4. ПО для настройки.

ПОРВ занимает целиком одно ядро процессора и 100-200 Мб ОЗУ. Все остальные ресурсы доступны для ПО следующего уровня.

### ПО реального времени (ПОРВ)

Язык написания C/C++ с патчем Xenomai. ПО плотно работает с железом. Задачи:

1. Считывание оптических датчиков
2. Формирование команд на вращение шаговых двигателей
3. Формирование сигналов на сканирование
4. Построчный захват изображения, первичная его обработка
5. Получение печаемого изображения и формирование сигналов на печать
6. Взаимодейсвие с ПО верхнего уровня:
    * Выдача сканированных изображений
    * Получение печаемого изображения
    * Передача телеметрии

Взаимодейсвие с ПО верхнего уровня можно будет организовать чезер TCP/IP, fifo, shared memory.

### ПО распознавания билета

Задачи ПО:

1. Получить две отсканированных картинки билета (верх и низ). Формат изображения: например PNG.
2. Распознать нужные коды на билете, передаем их в Стойку для принятия решения.
3. Отдать статус в ПОРВ.

Взаимодейсвие с ПОРВ и получение картинок: TCP/IP, Unix socket, fifo, shared memory (нужное подчеркнуть).

Язык программирования: любой, что работает на Linux, желательно компилируемый. C# скорее всего в пролёте. 

Можно будет взаимодействовать с БД как удаленной, так и локальной типа Postgress/MariaDB (базы только в ОЗУ).

### ПО формирования изображения билета

Задачи ПО:

1. Прием информации о штрих-кодах из внешнего мира
2. Формирование изображения билета в соответствие с шаблоном
3. Передача изображения билета в ПОРВ
4. Обработка статуса обработки ПОРВ

Взаимодейсвие с ПОРВ и передача картинок: TCP/IP, Unix socket, fifo, shared memory (нужное подчеркнуть).

Язык программирования: любой, что работает на Linux, желательно компилируемый. C# скорее всего в пролёте. 

Можно будет взаимодействовать с БД как удаленной, так и локальной типа Postgress/MariaDB (базы только в ОЗУ).

### ПО для настройки

Задачи ПО:

1. Настройка шаблона билета
2. Разграничивание зон печати штрихкодов, статичных изображений
3. Настройка взаимодействия с внешним миром
4. Формирование JSON с настройками для ПОРВ

Язык программирования: любой, что работает на Linux. C# скорее всего в пролёте. Хорошим вариантом будет Web интерфейс.


## Организация работы

ПОРВ пишется сильно связано с железной платформой, так что на первом этапе можно будет сделать симулятор,
отдающий и принимающий картинки.

Всё остальное ПО можно писать независимо от наличия аппаратной платформы.

## Выбор метода межпроцессного взаимодействия

Собрать и протестировать на Zynq следующее: https://github.com/goldsborough/ipc-bench
