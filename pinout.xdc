set_property PACKAGE_PIN G20 [get_ports IIC0_scl_io]
set_property PACKAGE_PIN G19 [get_ports IIC0_sda_io]

set_property IOSTANDARD LVCMOS33 [get_ports IIC0_scl_io]
set_property IOSTANDARD LVCMOS33 [get_ports IIC0_sda_io]

# Bus Chain595
set_property PACKAGE_PIN T11 [get_ports MC_RESET]
set_property PACKAGE_PIN U13 [get_ports MC_DATA]
set_property PACKAGE_PIN T10 [get_ports MC_CLOCK]
set_property PACKAGE_PIN T12 [get_ports MC_LATCH]

set_property IOSTANDARD LVCMOS33 [get_ports MC_RESET]
set_property IOSTANDARD LVCMOS33 [get_ports MC_DATA]
set_property IOSTANDARD LVCMOS33 [get_ports MC_CLOCK]
set_property IOSTANDARD LVCMOS33 [get_ports MC_LATCH]


# Step motors
set_property PACKAGE_PIN M17 [get_ports MS_STEP[0]]
set_property PACKAGE_PIN M18 [get_ports MS_DIR[0]]
set_property PACKAGE_PIN K19 [get_ports MS_STEP[1]]
set_property PACKAGE_PIN J19 [get_ports MS_DIR[1]]
set_property PACKAGE_PIN K17 [get_ports MS_STEP[2]]
set_property PACKAGE_PIN K18 [get_ports MS_DIR[2]]
set_property PACKAGE_PIN J18 [get_ports MS_STEP[3]]
set_property PACKAGE_PIN H18 [get_ports MS_DIR[3]]

set_property IOSTANDARD LVCMOS33 [get_ports MS_STEP[0]]
set_property IOSTANDARD LVCMOS33 [get_ports MS_STEP[1]]
set_property IOSTANDARD LVCMOS33 [get_ports MS_STEP[2]]
set_property IOSTANDARD LVCMOS33 [get_ports MS_STEP[3]]
set_property IOSTANDARD LVCMOS33 [get_ports MS_DIR[0]]
set_property IOSTANDARD LVCMOS33 [get_ports MS_DIR[1]]
set_property IOSTANDARD LVCMOS33 [get_ports MS_DIR[2]]
set_property IOSTANDARD LVCMOS33 [get_ports MS_DIR[3]]


# ��������������� ������
set_property PACKAGE_PIN L15 [get_ports LED_SELECT[0]]
set_property PACKAGE_PIN L14 [get_ports LED_SELECT[1]]
set_property PACKAGE_PIN T14 [get_ports SOLENOID[0]]
set_property PACKAGE_PIN T15 [get_ports SOLENOID[1]]
set_property PACKAGE_PIN G17 [get_ports ENABLE_HV]

set_property IOSTANDARD LVCMOS33 [get_ports LED_SELECT[0]]
set_property IOSTANDARD LVCMOS33 [get_ports LED_SELECT[1]]
set_property IOSTANDARD LVCMOS33 [get_ports SOLENOID[0]]
set_property IOSTANDARD LVCMOS33 [get_ports SOLENOID[1]]
set_property IOSTANDARD LVCMOS33 [get_ports ENABLE_HV]

# ��������������� �����
set_property PACKAGE_PIN G18 [get_ports PL_SW[0]]
set_property PACKAGE_PIN K14 [get_ports PL_SW[1]]
set_property PACKAGE_PIN J14 [get_ports PL_SW[2]]

set_property IOSTANDARD LVCMOS33 [get_ports PL_SW[0]]
set_property IOSTANDARD LVCMOS33 [get_ports PL_SW[1]]
set_property IOSTANDARD LVCMOS33 [get_ports PL_SW[2]]
