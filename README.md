# Отладочная плата Zynq 

## Внешний вид 

Верх:

![Верх](images/BoardTop_0.png)


Низ:

![Низ](images/BoardBottom_0.png)

## Питание 

Основное питание подводится через разъем типа [KPJX-4S-S](http://www.kycon.com/Pub_Eng_Draw/KPJX-4S-S.pdf). 
Напряжение 24 В ±5%. Максимальный потребляемый ток 5 А (примерно).

На плате установлены импульсные стабилизаторы [LMR14050SDDAR](https://www.ti.com/product/LMR14050) (Texas Instruments), 
вырабатывающие напряжения 5,0 и 3,3 В из входного 24 В. Максимальный ток нагрузки по линиям: 3 А.   

Локальные напряжения питания для CIS (3.3 В), драйвера подсветки CIS (3.3 В) и генератора опорного напряжения для CIS (1.8 В).

 
## Основные микросхемы 

### Драйвер шаговых двигателей (4 канала)

Texas Instruments [DRV8825PWPR](https://www.ti.com/product/DRV8825).

Токоизмерительный резистор в цепи ISEN: 0.2 Ом. Диапазон регулировки порогового напряжения VREF от 0.27-0.3 до 3.3 В.

Управление выводами MODE\[0..2\], SLEEP, RESET, ENABLE, DECAY осуществляется через сдвиговый регистр 
[74HC595PW](https://www.nexperia.com/product/74HC595PW) и буфер [74LVC1G125GW](https://www.nexperia.com/product/74LVC1G125GW) 
(только вход DECAY).

Подключение выводов регистра: 

| 595 | DRV8825  | Описание |
| ----| -------- | ------|
| Q0  | MODE2    | Режим микрошага |
| Q1  | MODE1    | Режим микрошага | 
| Q2  | MODE0    | Режим микрошага |
| Q3  | ENBL     | 0 - Включение, 1 - выключение |
| Q4  | BUF OE   | 1 - Decay open, 0 - Decay == BUF A |
| Q5  | BUF A    | Управление Decay при BUF OE = 0 |
| Q6  | SLEEP    | 0 - Сон, 1 - нормальная работа |
| Q7  | RESET    | 0 - сброс индексной машины и отключение выходного H моста |

Входы драйвера STEP, DIR подключены напрямую к фабрике ПЛИС.

Выход FAULT через триггер Шмитта [74LVC3G17DP](https://www.nexperia.com/products/analog-logic-ics/asynchronous-interface-logic/schmitt-triggers/74LVC3G17DP.html) 
подключен к светодиоду и регистру [74HC165PW](https://www.nexperia.com/products/analog-logic-ics/i-o-expansion-logic/shift-registers/74HC165PW.html).

Подключение выводов к ПЛИС:

| Номер канала | Вход канала | Название в дизайне HDL | Разъём        | Вывод ПЛИС |
| ------------ | ----------- |  :-------------------: | ------------- | :--------: |
| 1            | STEP        | MS_STEP[0]             | IO_L8P_T1_35  | M17        | 
|              | DIR         | MS_DIR[0]              | IO_L8N_T1_35  | M18        |
| 2            | STEP        | MS_STEP[1]             | IO_L10P_T1_35 | K19        |
|              | DIR         | MS_DIR[1]              | IO_L10N_T1_35 | J19        |
| 3            | STEP        | MS_STEP[2]             | IO_L12P_T1_35 | K17        |
|              | DIR         | MS_DIR[2]              | IO_L12N_T1_35 | K18        |
| 4            | STEP        | MS_STEP[3]             | IO_L14P_T2_35 | J18        |
|              | DIR         | MS_DIR[3]              | IO_L14N_T2_35 | H18        |

### Драйвер коллекторного двигателя

Allegro Microsystems [A4952ELYTR-T](https://www.allegromicro.com/en/Products/motor-drivers/Brush-DC-Motor-Drivers/A4952-3.aspx).

Токоизмерительный резистор в цепи LSS: 0,2 Ом. Диапазон регулировки порогового напряжения VREF от ххх до ххх В.

Входы IN1, IN2 подключены к фабрике ПЛИС.

Выход FAULT через триггер Шмитта [74LVC3G17DP](https://www.nexperia.com/products/analog-logic-ics/asynchronous-interface-logic/schmitt-triggers/74LVC3G17DP.html)
подключен к светодиоду и регистру [74HC165PW](https://www.nexperia.com/products/analog-logic-ics/i-o-expansion-logic/shift-registers/74HC165PW.html).

Подключение выводов к ПЛИС:

| Вход микросхемы | Название в дизайне HDL | Разъём       | Вывод ПЛИС |
| --------------- | ---------------------- | ------       | -----------|
| IN1             | MB_IN1                 | IO_L6N_T0_35 |  |
| IN2             | MB_IN2                 | IO_L6P_T0_35 |  |

### Голова принтера

Термопечатающая головка Rohm KD2002-DEFW00A. 

Схема головки:
![Схема головки](images/Printhead.png)

* Количество точек: 448 (192 + 256);
* Питание логическое: 5 В;
* Ток логический: 56 мА; 
* Питание силовое:  24 В;
* Ток силовой: 15 А (пик);
* Частота клока: 16 МГц;
* Терморезистор R25 = 30 кОм (25°С), B = 3950K ±2%

Расчет сопротивления по следующей формуле: Rx = R25 * exp(B * (1/Tx - 1/T25)), T - абсолютная температура

Входы головки подключены через мошный буфер с преобразование уровней [74LVC4245APW](https://www.nexperia.com/products/analog-logic-ics/asynchronous-interface-logic/voltage-translators-level-shifters/74LVC4245APW.html)
к выводам фабрики ПЛИС.

Выходы головки (DO) подключены через буфер с преобразованием уровней [74LVC2T45DC](https://www.nexperia.com/products/analog-logic-ics/asynchronous-interface-logic/voltage-translators-level-shifters/74LVC2T45DC.html) 
к входам фабрики ПЛИС.

Подключение выводов:

| Принтер | Название в дизайне HDL   | Разъём | Вывод ПЛИС |
|----- |-----         |-----         |-----|
| DI1  | PrinterDI1   | IO_L3P_T0_35 |  |
| DI2  | PrinterDI2   | IO_L3N_T0_35 |  |
| CLK  | PrinterCLK   | IO_L2N_T0_35 |  |
| LAT  | PrinterLatch | IO_L4P_T0_35 |  |
| STB1 | PrinterStb1  | IO_L1N_T0_35 |  |
| STB2 | PrinterStb2  | IO_L2P_T0_35 |  |
| DO1  | PrinterDO1   | IO_L4N_T0_35 |  |
| DO2  | PrinterDO2   | IO_L1P_T0_35 |  |

Терморезистор TM по схеме делителя напряжения в нижней части. Верхний резистор сопротивлением 68 кОм подключен к цепи 3.3 В.
Напряжение на терморезисторе измеряется 16-битным АЦП [MCP3425A0T-E/CH](https://www.microchip.com/wwwproducts/en/MCP3425).
АЦП подключен к шине **IIC_PL**.


Подача силового питания осуществляется через электронный ключ на связке [LT1910ES8](https://www.analog.com/en/products/lt1910.html)
с транзистором [STL90N6F7](https://www.st.com/en/power-transistors/stl90n6f7.html)

Вход включения питания IN подключен к фабрике ПЛИС:

| Вход микросхемы | Название в дизайне HDL | Разъём        | Вывод ПЛИС |
| --------------- | ---------------------- | ------        | :--------: |
| IN              | ENABLE_HV              | IO_L16P_T2_35 | G17        |

Выход FAULT через триггер Шмитта [74LVC3G17DP](https://www.nexperia.com/products/analog-logic-ics/asynchronous-interface-logic/schmitt-triggers/74LVC3G17DP.html)
подключен к светодиоду и регистру [74HC165PW](https://www.nexperia.com/products/analog-logic-ics/i-o-expansion-logic/shift-registers/74HC165PW.html).

### Contact Image Sensor (CIS)

До двух сенсоров фирмы LiteON DL525-02UHE.

Характеристики:

* Ширина считывания: 54.0 мм;
* Разрешение: 1200 / 600 / 300 DPI;
* Количество пикселей: 2552 / 1276 / 638;
* Скорость сканирование: 0.8 / 0.4 / 0.2 мс на цвет;
* Тактовая частота: 4 МГц максимум;
* Один аналоговый выход;
* Напряжение питания: 3.3 В;
* Потребляемый ток: 100 мА максимум.

Питание напряжением 3.3 В осуществляется через стабилизатор [AP2114H-3.3TRG1](https://www.diodes.com/part/view/AP2114).
  
Цифровые входы CIS через буфер мошный буфер без преобразование уровней [74LVC4245APW](https://www.nexperia.com/products/analog-logic-ics/asynchronous-interface-logic/voltage-translators-level-shifters/74LVC4245APW.html)
к выводам фабрики ПЛИС.

Подключение выводов CIS_1:

| Вывод CIS | Название в дизайне HDL | Разъём        | Вывод ПЛИС |
|----       |----------              |---------      |----|
| SEL       | CIS1_SEL               | IO_L15N_T2_35 | |
| SI        | CIS1_SI                | IO_L17P_T2_35 | |
| CLK       | CIS1_CLK               | IO_L17N_T2_35 | |

Подключение выводов CIS_2:

| Вывод CIS | Название в дизайне HDL | Разъём        | Вывод ПЛИС |
|----       |----                    |----           |----|
| SEL       | CIS2_SEL               | IO_L19P_T3_35 | |
| SI        | CIS2_SI                | IO_L19N_T3_35 | |
| CLK       | CIS2_CLK               | IO_L21P_T3_35 | |

Аналоговые сигналы обоих CIS подключены к входам VINR и VIRG АЦП.
  
#### АЦП сенсора

Аналоговый фронтэнд сенсора представлен АЦП фирмы Analog Devices [AD9826KRSZ](https://www.analog.com/en/products/ad9826.html).

Все цифровые сигналы заведены на фабрику ПЛИС:

| Вывод АЦП | Название в дизайне HDL | Разъём        | Вывод ПЛИС |
|-----------|------------------------|---------------|------------|
| CDSCLK1   | ADC_CDSCLK1            | IO_L5P_T0_35  |  |
| CDSCLK2   | ADC_CDSCLK2            | IO_L5N_T0_35  |  |
| ADCCLK    | ADC_CLK                | IO_L7P_T1_35  |  |
| D0        | ADC_DATA0              | IO_L15P_T2_35 |  |
| D1        | ADC_DATA1              | IO_L13N_35    |  |
| D2        | ADC_DATA2              | IO_L13P_35    |  |
| D3        | ADC_DATA3              | IO_L11N_T1_35 |  |
| D4        | ADC_DATA4              | IO_L11P_T1_35 |  |
| D5        | ADC_DATA5              | IO_L9N_T1_35  |  |
| D6        | ADC_DATA6              | IO_L9P_T1_35  |  |
| D7        | ADC_DATA7              | IO_L7N_T1_35  |  |
| SCLK      | ADC_SCLK               | IO_L23P_T3_35 |  |
| SDATA     | ADC_SDATA              | IO_L23N_T3_35 |  |
| SLOAD     | ADC_SLOAD              | IO_L21N_T3_35 |  |

#### Источник тока светодиодов подсветки

Источник тока, управляемый напряжением сделан на двухканальном ЦАП [AD5627RBCPZ](https://www.analog.com/en/products/ad5627r.html)
и операционном усилителе [MCP6002T-I/SN](https://www.microchip.com/wwwproducts/en/MCP6002). ЦАП подключен к шине **IIC_PL**.
Вывод CLR подключен к выводу IO_L24P_T3_35 разъема (ххх ПЛИС). 


Цвет подсветки выбирается двумя релюшками. Сигналы управления реле подключены к ПЛИС:

| Вывод   | Название в дизайне HDL | Разъём        | Вывод ПЛИС | 
|---------|------------------------|---------------| :--------: |
| Select1 | LED_SELECT[0]          | IO_L22N_T3_35 | L15        |
| Select2 | LED_SELECT[1]          | IO_L22P_T3_35 | L14        |

Цвет в зависимости от состояния Select:

| Select1 | Select2 | Цвет |
|---------|---------|------|
| 0       | 0       | | 
| 0       | 1       | |
| 1       | 0       | |
| 1       | 1       | |

## Шины

Некоторые выводы ПЛИС логически огранизованы в шины:

* Шина типа SPI (на схеме Chain595) для связи с пятью выходными сдвиговыми регистрами [74HC595PW](https://www.nexperia.com/product/74HC595PW);
* Шина типа SPI (на схеме Chain165) для связи с тремя входными сдвиговыми регистрами [74HC165PW](https://www.nexperia.com/products/analog-logic-ics/i-o-expansion-logic/shift-registers/74HC165PW.html);
* Шина IIC_PL для связи с АЦП термодатчика принтера, ЦАП драйвера светодиодов и памяти FRAM [FM24CL16B-GTR](https://www.cypress.com/part/fm24cl16b-gtr);
* Шина IIC1_PS (процессор ARM) для связи с памятью MAC адреса.

### Шина IIC_PS

Шина IIC1_PS подключена напрямую к процессорному ядру (Processing System, PS).
На шине стоит одно устройство: микросхема MAC адреса [24AA02E48T-I/OT](https://www.microchip.com/wwwproducts/en/24AA02E48).

| Вывод       | Название в дизайне HDL | Разъём       | Вывод ПЛИС | 
|-------------|------------------------|--------------| :--------: |
| IIC1_PS.SCL | Нет имени              | PS_MIO12_500 | D9         |
| IIC1_PS.SDA | Нет имени              | PS_MIO13_500 | E8         |


##### MAC адрес 24AA02E48T-I/OT

### Шина регистров 595 (Chain595)

Шина последовательного доступа к регистрам Serial-in - Parallel-out типа HC595. 
Шина подключаестся к ПЛИС через буфер 74LVC4245.

| Вывод          | Название в дизайне HDL | Разъём       | Вывод ПЛИС | 
|----------------|------------------------|--------------|------------|
| Chain595.RESET | MC_RESET               | IO_L1P_T0_34 | T11        |
| Chain595.DATA  | MC_DATA                | IO_L3P_T0_34 | U13 (PU)   |
| Chain595.CLOCK | MC_CLOCK               | IO_L1N_T0_34 | T10        |
| Chain595.LATCH | MC_LATCH               | IO_L2P_T0_34 | T12        | 


# Software

Смотри в [Software.md](Software.md)
